//
// Created by Alessandro Arfaioli on 04/02/2021.
//

#ifndef PROGETTO3_MAINLIB_H
#define PROGETTO3_MAINLIB_H

#include <math.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <time.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/wait.h>

#define BUFFERSIZE 128
#define WAITTIME 7

#define SOCKET_MASTER "sock0"
#define SOCKET_MANAGER "sockm"
#define SOCKET_TRANSDUCER "sockt"
#define SOCKET_WES "sockw"

#define CHANNEL_SOCKET "chSocket"
#define CHANNEL_PIPE "chPipe"
#define CHANNEL_FILE "chFile"

#define SIG_CONT "CONT"
#define SIG_TERM "TERM"
#define SIG_SUCC "SUCC"
#define SIG_FAIL "FAIL"


void master_init();

void master_setup();

void master_preparation();

void master_connection();

void master_routine();

void master_exit();

//

void createProcess(char *p_name, char *arg, int *pid_address) {
    int pid = fork();
    if (pid == 0) {
        if (arg == NULL) execl(p_name, NULL);
        else execl(p_name, arg, NULL);
    } else if (pid > 0) {
        *pid_address = pid;
    } else {
        perror("fork");
    }
}

//

void createSocket(char *s_name, int max_request, int *sfd_address) {
    unlink(s_name);
    int sock = socket(AF_UNIX, SOCK_STREAM, 0);
    struct sockaddr_un address;
    memset(&address, '\0', sizeof(address));
    address.sun_family = AF_UNIX;
    strcpy(address.sun_path, s_name);
    if (bind(sock, (struct sockaddr *) &address, sizeof(address)) == 0) {
        listen(sock, max_request);
        *sfd_address = sock;
    } else *sfd_address = -1;
}

//


void connectSocket(char *s_name, int *sfd_address) {
    int sock = socket(AF_UNIX, SOCK_STREAM, 0);
    struct sockaddr_un address;
    memset(&address, '\0', sizeof(address));
    address.sun_family = AF_UNIX;
    strcpy(address.sun_path, s_name);
    int connectionResult, timeout = WAITTIME;
    do {
        connectionResult = connect(sock, (struct sockaddr *) &address, sizeof(address));
        sleep(1);
    } while (connectionResult != 0 && timeout-- > 0);
    *sfd_address = connectionResult == 0 ? sock : -1;
}

//

int parseInt(char *buffer) {
    if(buffer == NULL || strlen(buffer) == 0) return 0;
    return (int) strtol(buffer, NULL, 10);
}

double parseDouble(char *buffer) {
    if(buffer == NULL || strlen(buffer) == 0) return 0;
    return strtod(buffer, NULL);
}

//

void sendSignal(int channel, char *signal) {
    write(channel, signal, BUFFERSIZE);
}

int gotCONTSignal(char *buffer) {
    return strcmp(buffer, SIG_CONT) == 0;
}

int gotTERMSignal(char *buffer) {
    return strcmp(buffer, SIG_TERM) == 0;
}

int gotSUCCSignal(char *buffer) {
    return strcmp(buffer, SIG_SUCC) == 0;
}


#endif //PROGETTO3_MAINLIB_H
