//
// Created by Alessandro Arfaioli on 04/02/2021.
//

#ifndef PROGETTO3_CHILDLIB_H
#define PROGETTO3_CHILDLIB_H

#include <signal.h>

// Da implementare

int setup();

int preparation();

int connection();

int routine();

void closeProcess();

// Implementate

void connectToMaster(int *channel, char *buffer);

void waitResponse(int *channel, char *buffer, int errors);

char *toSignal(int errors);

void startRoutine(int *channel, char *buffer) {
    while (1) {
        read(*channel, buffer, BUFFERSIZE);
        if (gotCONTSignal(buffer)) {
            int result = routine();
            write(*channel, toSignal(result), BUFFERSIZE);
        } else if (gotTERMSignal(buffer)) break;
        else {
            printf("child - start routine - BAD SIGNAL\n");
            exit(1);
        }
    }
}

void childMain(int *channel, char *buffer) {
    connectToMaster(channel, buffer);
    waitResponse(channel, buffer, setup());
    waitResponse(channel, buffer, preparation());
    waitResponse(channel, buffer, connection());
    startRoutine(channel, buffer);
    closeProcess();
}

void connectToMaster(int *channel, char *buffer) {
    connectSocket(SOCKET_MASTER, channel);
    sprintf(buffer, "%d", getpid());
    write(*channel, buffer, BUFFERSIZE);
    read(*channel, buffer, BUFFERSIZE);
    if (!gotCONTSignal(buffer)) closeProcess();
}

void waitResponse(int *channel, char *buffer, int errors) {
    sprintf(buffer, "%s", errors == 0 ? SIG_SUCC : SIG_FAIL);
    write(*channel, buffer, BUFFERSIZE);
    read(*channel, buffer, BUFFERSIZE);
    if (!gotCONTSignal(buffer)) closeProcess();
}

char *toSignal(int errors) {
    return errors == 0 ? SIG_SUCC : SIG_FAIL;
}

#endif //PROGETTO3_CHILDLIB_H
