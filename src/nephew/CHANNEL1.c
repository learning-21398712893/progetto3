#include "../../lib/mainlib.h"
#include "../../lib/nephewlib.h"

int transducerChannel;
char buffer[BUFFERSIZE];

FILE *myLog;

int mySocket, pfcChannel;

// ----------------------------------------------------------------------------------------- Main
int main(int argc, char **argv) {
    channelMain(&transducerChannel, buffer);
}

int setup() {
    myLog = fopen("../logs/speedPFC1.log", "w+");
    return myLog == NULL;
}

int preparation() {
    createSocket(CHANNEL_SOCKET, 1, &mySocket);
    pfcChannel = accept(mySocket, NULL, NULL);
    return pfcChannel <= 0;
}

int connection() {
    // do nothing
    return 0;
}

int routine() {
    memset(buffer, '\0', BUFFERSIZE);
    read(pfcChannel, buffer, BUFFERSIZE);
    fprintf(myLog, "%s\n", buffer);
    fflush(myLog);
    return 0;
}

void closeProcess() {
    close(mySocket);
    fclose(myLog);
    remove(CHANNEL_SOCKET);
    close(transducerChannel);
    exit(0);
}
