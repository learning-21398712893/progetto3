#include "../../lib/mainlib.h"
#include "../../lib/nephewlib.h"

int managerChannel;
char buffer[BUFFERSIZE];

int channel; // socket
int multiplier = 1;

int it = 0;
double lastLon, lastLat;

// ----------------------------------------------------------------------------------------- Headers

void sendResultToChannel(double result, int time);

void signal_handler(int signum) {
    if (signum == SIGUSR1) multiplier = 4;
}

// ----------------------------------------------------------------------------------------- Main

int main() {
    signal(SIGUSR1, signal_handler);
    pfcMain(&managerChannel, buffer);
}

int setup() {
    // do nothing
    return 0;
}

int preparation() {
    connectSocket(CHANNEL_SOCKET, &channel);
    return channel <= 0;
}

int connection() {
    // do nothing
    return 0;
}

int routine() {
    read(managerChannel, buffer, BUFFERSIZE);
    double newLon, newLat;
    int time;
    extractValues(buffer, &newLon, &newLat, &time);
    double result = it++ > 0 ? distanceBetween(lastLon, lastLat, newLon, newLat) : 0;
    sendResultToChannel(result * multiplier, time);
    multiplier = 1;
    lastLon = newLon;
    lastLat = newLat;
    memset(buffer, '\0', BUFFERSIZE);
    return 0;
}

void closeProcess() {
    close(channel);
    close(managerChannel);
    exit(0);
}

// ----------------------------------------------------------------------------------------- Complementary

void sendResultToChannel(double result, int time) {
    sprintf(buffer, "%d: %f", time, result);
    write(channel, buffer, BUFFERSIZE);
    bzero(buffer, BUFFERSIZE);
}
