#include "../../lib/mainlib.h"
#include "../../lib/nephewlib.h"

int transducerChannel;
char buffer[BUFFERSIZE];

FILE *myLog;

FILE *myFile;

// ----------------------------------------------------------------------------------------- Main

int main(int argc, char **argv) {
    channelMain(&transducerChannel, buffer);
}

int setup() {
    myLog = fopen("../logs/speedPFC3.log", "w+");
    return myLog == NULL;
}

int preparation() {
    myFile = fopen(CHANNEL_FILE, "w+");
    return myFile == NULL;
}

int connection() {
    // do nothing
    return 0;
}

int routine() {
    memset(buffer, '\0', BUFFERSIZE);
    fseek(myFile, 0, SEEK_CUR);
    fgets(buffer, BUFFERSIZE, myFile);
    fprintf(myLog, "%s\n", buffer);
    fflush(myLog);
    return 0;
}

void closeProcess() {
    fclose(myFile);
    fclose(myLog);
    remove(CHANNEL_SOCKET);
    close(transducerChannel);
    exit(0);
}
