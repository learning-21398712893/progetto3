#include "../../lib/mainlib.h"
#include "../../lib/nephewlib.h"

int managerChannel;
char buffer[BUFFERSIZE];

FILE *channel; // file
int multiplier = 1;

int it = 0;
double lastLon, lastLat;

// ----------------------------------------------------------------------------------------- Headers

void sendResultToChannel(double result, int time);

void signal_handler(int signum) {
    if (signum == SIGUSR1) multiplier = 4;
}

// ----------------------------------------------------------------------------------------- Main

int main() {
    signal(SIGUSR1, signal_handler);
    pfcMain(&managerChannel, buffer);
}

int setup() {
    // do nothing
    return 0;
}

int preparation() {
    int timeout = WAITTIME;
    while (timeout-- > 0 && channel == NULL) {
        channel = fopen(CHANNEL_FILE, "a");
        sleep(1);
    }
    int result = channel == NULL;
    return result;
}

int connection() {
    // do nothing
    return 0;
}

int routine() {
    bzero(buffer, BUFFERSIZE);
    read(managerChannel, buffer, BUFFERSIZE);
    double newLon, newLat;
    int time;
    extractValues(buffer, &newLon, &newLat, &time);
    double result = it++ > 0 ? distanceBetween(lastLon, lastLat, newLon, newLat) : 0;
    sendResultToChannel(result * multiplier, time);
    multiplier = 1;
    lastLon = newLon;
    lastLat = newLat;
    memset(buffer, '\0', BUFFERSIZE);
    return 0;
}

void closeProcess() {
    fclose(channel);
    close(managerChannel);
    exit(0);
}

// ----------------------------------------------------------------------------------------- Complementary

void sendResultToChannel(double result, int time) {
    fprintf(channel, "%d: %f", time, result);
    fflush(channel);
    bzero(buffer, BUFFERSIZE);
}
