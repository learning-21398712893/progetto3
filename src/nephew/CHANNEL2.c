#include "../../lib/mainlib.h"
#include "../../lib/nephewlib.h"

int transducerChannel;
char buffer[BUFFERSIZE];

FILE *myLog;

int myPipe;

// ----------------------------------------------------------------------------------------- Main

int main(int argc, char **argv) {
    channelMain(&transducerChannel, buffer);
}

int setup() {
    myLog = fopen("../logs/speedPFC2.log", "w+");
    return myLog == NULL;
}

int preparation() {
    createNonBlockingPipe(CHANNEL_PIPE, &myPipe);
    return myPipe <= 0;
}

int connection() {
    // do nothing
    return 0;
}

int routine() {
    memset(buffer, '\0', BUFFERSIZE);
    read(myPipe, buffer, BUFFERSIZE);
    fprintf(myLog, "%s\n", buffer);
    fflush(myLog);
    return 0;
}

void closeProcess() {
    close(myPipe);
    fclose(myLog);
    remove(CHANNEL_SOCKET);
    close(transducerChannel);
    exit(0);
}
