#include "../../lib/mainlib.h"
#include "../../lib/childlib.h"

#define PROB(n) (rand() % (n)) == 0

int masterChannel;
char buffer[BUFFERSIZE];

int PFC[3];

FILE *logFile;

// ----------------------------------------------------------------------------------------- Main

int main() {
    srand(time(0));
    childMain(&masterChannel, buffer);
}

int setup() {
    logFile = fopen("../logs/failures.log", "w");
    return logFile == NULL;
}

int preparation() {
    // do nothing
    return 0;
}

int connection() {
    read(masterChannel, buffer, BUFFERSIZE);
    char *pfc = strtok(buffer, " ");
    int i = 0;
    while (pfc != NULL) {
        PFC[i++] = parseInt(pfc);
        pfc = strtok(NULL, " ");
    }
    return PFC[0] <= 0 && PFC[1] <= 0 && PFC[2] <= 0;
}

int routine() {
    int index = rand() % 3;
    int pid = PFC[index];
    int signalsSend = 0;
    if (PROB(100)) {
        kill(pid, SIGSTOP);
        fprintf(logFile, "PFC%d | Inviato SIGSTOP\n", index + 1);
        signalsSend++;
    }
    if (PROB(10000)) {
        kill(pid, SIGINT);
        fprintf(logFile, "PFC%d | Inviato SIGINT\n", index + 1);
        signalsSend++;
    }
    if (PROB(10)) {
        kill(pid, SIGCONT);
        fprintf(logFile, "PFC%d | Inviato SIGCONT\n", index + 1);
        signalsSend++;
    }
    if (PROB(10)) {
        kill(pid, SIGUSR1);
        fprintf(logFile, "PFC%d | Inviato SIGUSR1\n", index + 1);
        signalsSend++;
    }
    if (signalsSend == 0) fprintf(logFile, "PFC%d | Nessun segnale inviato\n", index + 1);
    fprintf(logFile, "-------------------------------------------\n");
    fflush(logFile);
    return 0;
}

void closeProcess() {
    fclose(logFile);
    close(masterChannel);
    exit(0);
}
