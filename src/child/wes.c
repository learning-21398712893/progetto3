#include "../../lib/mainlib.h"
#include "../../lib/childlib.h"

int masterChannel;
char buffer[BUFFERSIZE];

FILE *logFile; // status.log
int mySocket, switchChannel; // wes channel

FILE *inputLogs[3]; // speedPFC1.log , speedPFC2.log , speedPFC3.log ,

// ----------------------------------------------------------------------------------------- Headers

void extractFromFile(FILE *file, double *speed, int *time);

int evalBrokenPfc(double s1, double s2, double s3);

void signalSwitch(char *string);

// ----------------------------------------------------------------------------------------- Main

int main() {
    childMain(&masterChannel, buffer);
}

int setup() {
    logFile = fopen("../logs/status.log", "w+");
    createSocket(SOCKET_WES, 1, &mySocket);
    return logFile == NULL || mySocket <= 0;
}

int preparation() {
    inputLogs[0] = fopen("../logs/speedPFC1.log", "r");
    inputLogs[1] = fopen("../logs/speedPFC2.log", "r");
    inputLogs[2] = fopen("../logs/speedPFC3.log", "r");
    int result = inputLogs[0] == NULL || inputLogs[1] == NULL || inputLogs[2] == NULL;
    switchChannel = accept(mySocket, NULL, NULL);
    return result || switchChannel <= 0;
}

int connection() {
    // do nothing
    return 0;
}

int routine() {
    double pfc1Speed, pfc2Speed, pfc3Speed;
    int pfc1Time, pfc2Time, pfc3Time;
    extractFromFile(inputLogs[0], &pfc1Speed, &pfc1Time);
    extractFromFile(inputLogs[1], &pfc2Speed, &pfc2Time);
    extractFromFile(inputLogs[2], &pfc3Speed, &pfc3Time);
    int count = (pfc1Speed == pfc2Speed) + (pfc2Speed == pfc3Speed) + (pfc1Speed == pfc3Speed);
    char result[32];
    memset(&result, '\0', 32);
    if (count == 3) {
        signalSwitch("");
        strcpy(result, "OK");
    } else if (count == 1) {
        int pfcBroken = evalBrokenPfc(pfc1Speed, pfc2Speed, pfc3Speed);
        sprintf(result, "%d %d", pfcBroken, pfc1Time);
        signalSwitch(result);
        strcpy(result, "ERRORE");
    } else {
        sprintf(result, "0 %d", pfc1Time);
        signalSwitch(result);
        strcpy(result, "EMERGENZA");
    }
    sprintf(buffer, "%d: %s\n", pfc1Time, result);
    int sec = pfc1Time % 100;
    int min = (pfc2Time / 100) % 100;
    int hour = (pfc3Time / 10000) % 100;
    printf("[%s%d:%s%d:%s%d] %s\n",
           hour < 10 ? "0" : "", hour,
           min < 10  ? "0" : "", min,
           sec < 10  ? "0" : "", sec,
           result);
    fprintf(logFile, "%s", buffer);
    fflush(logFile);
    return 0;
}

void closeProcess() {
    fclose(logFile);
    fclose(inputLogs[0]);
    fclose(inputLogs[1]);
    fclose(inputLogs[2]);
    close(switchChannel);
    close(mySocket);
    close(masterChannel);
    exit(0);
}

// ----------------------------------------------------------------------------------------- Complementary

void extractFromFile(FILE *file, double *speed, int *time) {
    fflush(file);
    memset(buffer, '\0', BUFFERSIZE);
    fseek(file, 0, SEEK_CUR);
    fgets(buffer, BUFFERSIZE, file);
    *time = parseInt(strtok(buffer, ": "));
    *speed = parseDouble(strtok(NULL, " "));
}

int evalBrokenPfc(double s1, double s2, double s3) {
    int diff[3] = {s1 == s2, s2 == s3, s3 == s1};
    if (diff[0] == 1) return 3;
    else if (diff[1] == 1) return 1;
    else if (diff[2] == 1) return 2;
    return 0;
}

void signalSwitch(char *string) {
    write(switchChannel, string, BUFFERSIZE);
}