#include "../../lib/mainlib.h"
#include "../../lib/childlib.h"

int masterChannel;
char buffer[BUFFERSIZE];

FILE *logFile;

int wesChannel;

int PFC[3];

// ----------------------------------------------------------------------------------------- Headers

void extractWesCall(int *value, int *time);

void checkPfcStatus(int pfcNumName);

int isProcessBlocked(int pid);

int isProcessDead(int pid);

// ----------------------------------------------------------------------------------------- Main


int main() {
    childMain(&masterChannel, buffer);
}

int setup() {
    logFile = fopen("../logs/switch.log", "w");
    return logFile == NULL;
}

int preparation() {
    connectSocket(SOCKET_WES, &wesChannel);
    return wesChannel <= 0;
}

int connection() {
    read(masterChannel, buffer, BUFFERSIZE);
    char *pfc = strtok(buffer, " ");
    int i = 0;
    while (pfc != NULL) {
        PFC[i++] = parseInt(pfc);
        pfc = strtok(NULL, " ");
    }
    return PFC[0] <= 0 && PFC[1] <= 0 && PFC[2] <= 0;
}

int routine() {
    // Possibili input :
    // NULL = OK
    // X TIME = PFCX rotto al momento TIME (x>0)
    // 0 TIME = EMERGENZA al momento TIME
    bzero(buffer, BUFFERSIZE);
    read(wesChannel, buffer, BUFFERSIZE);
    if (strlen(buffer) == 0) return 0; // no probles from WES
    int pfcNumName, time;
    extractWesCall(&pfcNumName, &time);
    if (pfcNumName == 0) {
        fprintf(logFile, "EMERGENZA [%d]\n", time);
        fflush(logFile);
        return 1;
    } else {
        fprintf(logFile, "ERRORE [time %d][PFC%d]", time, pfcNumName);
        fflush(logFile);
        checkPfcStatus(pfcNumName);
        return 0;
    }
}

void closeProcess() {
    close(masterChannel);
    exit(0);
}

// ----------------------------------------------------------------------------------------- Complementary

void extractWesCall(int *value, int *time) {
    char tempBuffer[16];
    strcpy(tempBuffer, buffer);
    char *temp = strtok(tempBuffer, " ");
    int pfcIndex = parseInt(temp);
    temp = strtok(NULL, "%");
    int timeValue = parseInt(temp);
    *value = pfcIndex;
    *time = timeValue;
}

void checkPfcStatus(int pfcNumName) {
    int processPid = PFC[pfcNumName - 1];
    if (isProcessBlocked(processPid)) {
        kill(processPid, SIGCONT);
        fprintf(logFile, "[pid %d] - Processo interrotto e ripristinato\n", processPid);
    } else if (isProcessDead(processPid)) {
        fprintf(logFile, "[pid %d] - Processo terminato\n", processPid);
    } else {
        fprintf(logFile, "[pid %d] - Errore di comunicazione coordinate\n", processPid);
    }
    fflush(logFile);
}

int isProcessBlocked(int pid) {
    // TODO controllo stato del processo bloccato
    // return 1 se bloccato, return 0 altrimenti
    return 1;
}

int isProcessDead(int pid) {
    // TODO controllo stato del processo terminato
    // return 1 se terminato, return 0 altrimenti
    return 1;
}
